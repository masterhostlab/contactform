<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Us</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    
</head>
<style>
// Remy function
@function remy($value) {
  @return ($value / 16px) * 1rem;
}

body {
  font: 100% / 1.414 "Open Sans", "Roboto", arial, sans-serif;
  background: #e9e9e9;
}
a,
[type="submit"] {transition: all .25s ease-in;}
.signup__container {
  position: absolute;
  top: 50%;
  right: 0;
  left: 0;
  margin-right: auto;
  margin-left: auto;
  transform: translateY(-50%);
  /* overflow: hidden; */
  display: flex;
  align-items: center;
  justify-content: center;
  width: auto-width;
  height: 600px;
  border-radius: remy(3px);
  box-shadow: 0px remy(3px) remy(7px) rgba(0,0,0,.25);
}
.signup__overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #faeb1b;
}
.container__child {
  width: 50%;
  height: 100%;
  color: #fff;
}
.signup__thumbnail {
  position: relative;
  padding: 2rem;
  /* display: flex; */
  flex-wrap: wrap;
  align-items: center;
  /* background: url(http://ultraimg.com/images/spectre-login.jpg); */
  background-repeat: no-repeat;
  background-position: top center;
  background-size: cover;
}
.thumbnail__logo,
.thumbnail__content,
.thumbnail__links {
  position: relative;
  z-index: 2;  
}
.thumbnail__logo {align-self: flex-start;}
.logo__shape {fill: #fff;}
.logo__text {
  display: inline-block;
  font-size: .8rem;
  font-weight: 700;
  vertical-align: bottom;
}
.thumbnail__content {align-self: center;}
/* h1,
h2 {
  font-weight: 500;
  color: #013975;
} */
h1{
  font-weight: 900;
  font-size: 70px;
  color: #013e7c;
}
.heading--primary {
font-size: 1.999rem;
}
.heading--secondary {font-size: 1.414rem;}
.thumbnail__links {
  align-self: flex-end;
  width: 100%;
}
.thumbnail__links a {
  font-size: 1rem;
  color: #fff;
  &:focus,
  &:hover {color: rgba(255,255,255,.5);}
}
.signup__form {
  padding: 6.5rem;
  background: #fafafa;
}
label {
  font-size: .85rem;
  text-transform: uppercase;
  color: #ccc;
}
.form-control {
  background-color: transparent;
  border-top: 0;
  border-right: 0;
  border-left: 0;
  border-radius: 0;
  &:focus {border-color: #111;}
}
[type="text"] {color: #111;}
[type="password"] {color: #111;}
.btn--form {
  padding: .5rem 2.5rem;
  font-size: .95rem;
  font-weight: 600;
  text-transform: uppercase;
  color: #fff;
  background: #111;
  border-radius: remy(35px);
  &:focus,
  &:hover {background: lighten(#111, 13%);}
}
.signup__link {
  font-size: .8rem;
  font-weight: 600;
  text-decoration: underline;
  color: #999;
  &:focus,
  &:hover {color: darken(#999, 13%);}
}
.normal {
  border-top: 12px solid #013e7c;
}
.bottomnormal{
  border-bottom: 27px solid #013e7c;
  margin-top: 550px;
}
.btn-colour-1 {
  color: #f72ce0;
  border-radius: 0;
}
.btn-colour-1 {
  color: #ffffff;
  background-color: #f72ce0;
  border-color: #f72ce0;
  letter-spacing: 0.05em;
  border-radius: 0;
}
.error {color: #FF0000;}
.parsley-errors-list li{
  list-style: none;
  color: red;
}


</style>
<body>
<div class="normal mt-2"></div>
<div class="signup__container">

  <div class="container__child signup__thumbnail">
    <div class="thumbnail__content text-center">
      <h1 style="padding-left: 50px; padding-top:100px;" class="mt-4">Let us know</h1>
      <h1 style="margin-left: -70px;" class="mt-4">what you</h1>
      <h1 style="margin-left: -200px;" class="mt-4">think!</h1>
    </div>
    <div class="signup__overlay"></div>
  </div>
  
  <div class="container__child signup__form">
  
  <form method="POST" id="contactForm" action="{{route('contact.send')}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="name" style="color: #013e7c;"><b>Name</b><span class="error">*</span></label>
        <input type="text" name="name" id="name" class="form-control" required />
        <span class="text-danger error-text name_err"></span>
    </div>
    <div class="form-group">
        <label for="email" style="color: #013e7c;"><b>Email</b><span class="error">*</span></label>
        <input type="text" name="email" id="email" class="form-control" required />
        <span class="text-danger error-text email_err"></span>
    </div>
    <div class="form-group">
        <label for="weburl" style="color: #013e7c;"><b>Web URL</b><span class="error">*</span></label>
        <input type="text" name="weburl" id="weburl" class="form-control" required/>
        <span class="text-danger error-text weburl_err"></span>
    </div>
    <div class="form-group">
        <label for="msg" style="color: #013e7c;"><b>Your Message to us</b><span class="error">*</span></label>
        <textarea name="msg" id="msg" class="form-control" required></textarea>
        <span class="text-danger error-text msg_err"></span>
    </div>
    <button type="submit" class="btn btn-colour-1 float-right">Submit</button>
    </form>  
  </div>
  
</div>
<div class="bottomnormal">hi</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script>
    // $(function(){
    //   $(".contactForm").parsley();
    // });

    $("#contactForm").submit(function(e){
        e.preventDefault();

        let name = $("#name").val();
        let email = $("#email").val();
        let weburl = $("#weburl").val();
        let msg = $("#msg").val();
        let _token = $("input[name=_token]").val();
        
        $.ajax({
            url: "{{route('contact.add')}}",
            type: "POST",
            data:{
                name:name,
                email:email,
                weburl:weburl,
                msg:msg,
                _token:_token
            },
            success: function(data) {
                  console.log(data.error)
                    if($.isEmptyObject(data.error)){
                        alert(data.success);
                    }else{
                        printErrorMsg(data.error);
                    }
                }
        });
    });

    function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            console.log(key);
              $('.'+key+'_err').text(value);
            });
        }
    });
</script>

</body>
</html>