<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use App\Models\Contact;
use Mail;
use Validator;

class ContactformController extends Controller
{
    public function index(){
        $formdetails = Contact::orderBy('id','DESC')->get();
        return view('contact-us',compact('formdetails'));
    }

    public function addContact(Request $request){
       
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->weburl = $request->weburl;
        $contact->msg = $request->msg;
        $contact->save();
       
        $details = [
            'name' => $request->name,
            'email' => $request->email,
            'weburl' => $request->weburl,
            'msg' => $request->msg,
        ];

        Mail::to('bhuvanameenakshi@gmail.com')->send(new ContactMail($details));
        return back()->with('message_sent','Your Message has sent successfully!');
        
    }
}
